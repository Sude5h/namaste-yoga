jQuery(document).ready(function($) {
  //Main slider
  //-----------------------------------------------
  //Owl carousel
  //-----------------------------------------------
  $(".owl-carousel").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });

  // Magnific popup
  //-----------------------------------------------
  if (
    $(".popup-img").length > 0 ||
    $(".popup-iframe").length > 0 ||
    $(".popup-img-single").length > 0
  ) {
    $(".popup-img").magnificPopup({
      type: "image",
      gallery: {
        enabled: true
      }
    });
    $(".popup-img-single").magnificPopup({
      type: "image",
      gallery: {
        enabled: false
      }
    });
    $(".popup-iframe").magnificPopup({
      disableOn: 700,
      type: "iframe",
      preloader: false,
      fixedContentPos: false
    });
  }
  //Scroll totop
  //-----------------------------------------------//
  $(window).scroll(function() {
    if ($(this).scrollTop() !== 0) {
      $(".scrollToTop").fadeIn();
    } else {
      $(".scrollToTop").fadeOut();
    }
  });

  $(".scrollToTop").click(function() {
    $("body,html").animate({ scrollTop: 0 }, 800);
  });
  AOS.init();
});
jQuery(window).ready(function() {});

//Scroll totop
//-----------------------------------------------//
$(window).scroll(function() {
  if ($(this).scrollTop() !== 0) {
    $(".scrollToTop").fadeIn();
  } else {
    $(".scrollToTop").fadeOut();
  }
});

$(".scrollToTop").click(function() {
  $("body,html").animate({ scrollTop: 0 }, 800);
});

// SIGN UP FORM
function toggleResetPswd(e){
  e.preventDefault();
  $('#logreg-forms .form-signin').toggle() // display:block or none
  $('#logreg-forms .form-reset').toggle() // display:block or none
}

function toggleSignUp(e){
  e.preventDefault();
  $('#logreg-forms .form-signin').toggle(); // display:block or none
  $('#logreg-forms .form-signup').toggle(); // display:block or none
}

$(()=>{
  // Login Register Form
  $('#logreg-forms #forgot_pswd').click(toggleResetPswd);
  $('#logreg-forms #cancel_reset').click(toggleResetPswd);
  $('#logreg-forms #btn-signup').click(toggleSignUp);
  $('#logreg-forms #cancel_signup').click(toggleSignUp);
})